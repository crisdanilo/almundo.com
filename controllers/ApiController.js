import Controllers from '../app/libs/Controllers';
import Hotels from '../models/Hotels';

export default class ApiController extends Controllers
{
    constructor() {
        super();
    }

    getAll(req, res) {
        super.response(new Hotels, res);
    }

    getOne(req, res) {
        const id = req.params.id;
        super.response(new Hotels, res, id);
    }
}
