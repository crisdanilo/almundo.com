'use strict';
import ApiController from '../controllers/ApiController';
const api = new ApiController;

export const Routes = (router) => {
    router.get('/hotels', api.getAll);
    router.get('/hotels/:id', api.getOne);
};
