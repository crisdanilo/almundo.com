import Connection from '../bin/Connection';

export default class Models
{
    constructor() {
        if (this.constructor === Models) {
            throw new TypeError('Can not construct abstract class.');
        }

        this.connection = new Connection;
    }

    getJSON(where = null, callback) {
        const fields = Array.isArray(this.fields) ? this.fields.join(', ') : this.fields;
        let query = `SELECT ${fields} FROM ${this.table}`;

        if (where) {
            query += ` WHERE id = ${where}`;
        }

        this.connection.execute(query, (err, rows) => {
            if (!err) {
                callback(null, rows);
            } else {
                callback(err, null);
            }
        });
    }
}

Models.prototype.table = '';
Models.prototype.fields = '*';
