import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import url from 'url';
import { Routes } from './Routes';

let instance = null;

export default class App
{
    constructor() {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    start() {
        this._configure();
        this.app.listen(3000, () => {
            console.log('Node server running on http://localhost:3000');
        });
    }

    render() {
        const router = express.Router();
        this.app.use('/api/v1', router);

        this.app.use((req, res, next) => {
            const err = new Error('Not Found');
            err.status = 404;
            next(err);
        });
        Routes(router);
    }

    _configure() {
        this.app = express();
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(helmet());
        this.app.disable('x-powered-by');
        this.app.all('/*', (req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header('Access-Control-Allow-Methods', 'HEAD, GET, PUT, POST, DELETE, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-type, Accept');
            res.header('Content-type', 'application/json');
            this.handle(req, res, next);
        })
    }

    handle(req, res, next) {
        let path = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl,
        });
        console.log(`${req.method} ${res.statusCode} ${path}`);
        this.render();
        if (req.method === 'OPTIONS') {
            res.status(200).end();
        } else {
            next();
        }
    }
}

App.prototype.app = {};
